[![python](https://www.python.org/static/community_logos/python-logo-master-v3-TM.png)](https://www.python.org/)

Programming with Python
=====

Teaching materials for the course "Programming with Python" (894003) at [BOKU](https://boku.ac.at/)

### License
[![BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)


This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).
