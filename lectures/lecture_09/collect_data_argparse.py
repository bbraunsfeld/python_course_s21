import numpy as np
import pandas as pd

import argparse

parser = argparse.ArgumentParser(description='collect data from different files')
parser.add_argument('-files', help="list of files", nargs='+', required = True)
parser.add_argument('-o', type=str, help="output file")
parser.add_argument('-norm', default=False, action='store_true', help="normalize data")

args = parser.parse_args()

data = []
for i, f_path in enumerate(args.files):
    f = open(f_path)
    x = []
    y = []
    flag_data = False
    for l in f:
        if flag_data:
            temp = l.split()
            x.append(float(temp[0]))
            y.append(float(temp[1]))
        if l.startswith('data'):
            flag_data = True
    if i == 0:
        data.append(x)
    data.append(y)

data = np.array(data).T
if args.norm:
    for i in range(1, data.shape[1]):
        data[:,i] /= data[:,i].sum()

df = pd.DataFrame(data)
cols = ['x'] + ['data_' + str(i+1) for i in range(len(args.files))]
df.columns = cols

df.to_csv(args.o)
