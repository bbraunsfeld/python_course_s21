"""read multiple files and collect/combine the data

Author: name
Date: 1.1.2000"""

import numpy as np
import pandas as pd

def read_data(f_path):
    f = open(f_path)
    x = []
    y = []
    flag_data = False
    for l in f:
        if flag_data:
            temp = l.split()
            x.append(float(temp[0]))
            y.append(float(temp[1]))
        if l.startswith('data'):
            flag_data = True
    return x, y

def read_files(file_list):
    data = []
    for i, f_path in enumerate(file_list):
        x, y = read_data(f_path)
        if i == 0:
            data.append(x)
        data.append(y)
    data = np.array(data).T
    return data

def normalize(data):
    for i in range(1, data.shape[1]):
        data[:,i] /= data[:,i].sum()

def create_df(data):
    df = pd.DataFrame(data)
    cols = ['x'] + ['data_' + str(i) for i in range(1, data.shape[1])]
    df.columns = cols
    return df 

def save_data(output_file, data):
    df = create_df(data)
    df.to_csv(output_file)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='collect data from different files')
    parser.add_argument('-files', help="list of files", nargs='+', required = True)
    parser.add_argument('-o', type=str, help="output file")
    parser.add_argument('-norm', default=False, action='store_true', help="normalize data")

    args = parser.parse_args()

    data = read_files(args.files)
    if args.norm:
        normalize(data)
    save_data(args.o, data)
