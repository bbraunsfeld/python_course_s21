import argparse

parser = argparse.ArgumentParser(description='Sum input numbers')
parser.add_argument('-n1', type=int, help="first input integer number", required = True)
parser.add_argument('-n2', type=int, nargs='+', help="second input integer number") # 1 or more numbers
parser.add_argument('-n2_file', type=str, nargs='*', help="second input integer number (from file)") # 0 or more files
parser.add_argument('-abs', default=False, action='store_true', help="use absolute numbers (always positive)")

args = parser.parse_args()

print(args)
print('')
# code implementation
s = args.n1
for num in args.n2:
    if args.abs:
        num = abs(num)
    s+=num
# for loop going over n2_files to read the numbers and add to the sum...
print('final sum:', s)
