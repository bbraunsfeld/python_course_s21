import argparse

parser = argparse.ArgumentParser(description='Get some input integer number.')
parser.add_argument('-i', '--input', dest='input_num', type=int, help="input integer number")
args = parser.parse_args()

print(args.input_num)
