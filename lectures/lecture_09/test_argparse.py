import argparse

parser = argparse.ArgumentParser(description='Get some input.')
parser.add_argument('-i', help="input value")
args = parser.parse_args()

print(args.i)
